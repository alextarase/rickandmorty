import { createRouter, createWebHistory } from "vue-router";
import Characters from "../pages/Character/Characters.vue";
import Episodes from "../pages/Episode/Episodes.vue";
import Locations from "../pages/Location/Locations.vue";
import CharacterInfo from "../pages/Character/CharacterInfo.vue";
import EpisodeInfo from "../pages/Episode/EpisodeInfo.vue";
import LocationsInfo from "../pages/Location/LocationInfo.vue";
const routes = [
  {
    path: "/",
    component: Characters,
  },
  {
    path: "/episodes",
    component: Episodes,
  },
  {
    path: "/locations",
    component: Locations,
  },
  {
    path: "/characters/:id",
    component: CharacterInfo,
  },
  {
    path: "/episodes/:id",
    component: EpisodeInfo,
  },
  {
    path: "/locations/:id",
    component: LocationsInfo,
  },
];

const router = createRouter({
  routes,
  history: createWebHistory(),
});

export default router;
