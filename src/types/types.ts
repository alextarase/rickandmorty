export interface ICharacter {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: IOrigin;
  image: string;
  episode: String[];
}

export interface IOrigin {
  name: string;
  url: string;
}
export interface CharacterResponse {
  info: {
    count: number;
    next: string | null;
    pages: number;
    prev: string | null;
  };
  results: Array<ICharacter>;
}

export interface IEpisode {
  id: number;
  name: string;
  episode: string;
  air_date: string;
}

export interface EpisodeResponse {
  info: {
    count: number;
    next: string | null;
    pages: number;
    prev: string | null;
  };
  results: Array<IEpisode>;
}

export interface ILocation {
  id: number;
  name: string;
  type: string;
  dimension: string;
}

export interface LocationResponse {
  info: {
    count: number;
    next: string | null;
    pages: number;
    prev: string | null;
  };
  results: Array<ILocation>;
}

export interface ISortOptions {
  name: string;
  value: string;
}
